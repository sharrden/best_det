using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayLoop : MonoBehaviour
{
    [SerializeField] private AudioClip _loop;
    [SerializeField] private float _spatial;
    [SerializeField] private float _volume;
    [SerializeField] private float _pan;
    void Start()
    {
        SoundManager.Instance.spatialBlend = _spatial;
        SoundManager.Instance.volume = _volume;
        SoundManager.Instance.pan = _pan;
        SoundManager.Instance.Playloop(_loop);
        
    }
}
