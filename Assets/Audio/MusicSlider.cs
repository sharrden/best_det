using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicSlider : MonoBehaviour
{
    [SerializeField] private Slider _slider2;
    void Start()
    {
        _slider2.onValueChanged.AddListener(val => SoundManager.Instance.ChangeMusicVolume(val));
    }

    
}
