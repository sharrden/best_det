using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    [SerializeField] private AudioSource _musicSource, _effectSource, _loopSource;

    public float spatialBlend;
    public float volume;
    public float pan;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void Playsound(AudioClip clip)
        {
        _effectSource.spatialBlend = spatialBlend;
        _effectSource.volume = volume;
        _effectSource.panStereo = pan;

        _effectSource.PlayOneShot(clip);
 
        
        }
    public void Playloop(AudioClip loop)
    {
        _loopSource.loop = true;
        _loopSource.clip = loop;
        _loopSource.Play();
        _loopSource.spatialBlend = spatialBlend;
        _loopSource.volume = volume;
        _loopSource.panStereo = pan;

    }

    public void Playmusic(AudioClip bgmusic)
    {
        _musicSource.loop = true;
        _musicSource.clip = bgmusic;
        _musicSource.Play();
        _musicSource.spatialBlend = spatialBlend;
        _musicSource.volume = volume;
        _loopSource.panStereo = pan;


    }
    /// Volume sliders

    public void ChangeMusicVolume(float value) {
        _musicSource.volume = value;
    }
    public void ChangeMasterVolume(float value) {
        AudioListener.volume = value;
    }
    public void ChangeEffectVolume(float value) {
        _effectSource.volume = value;
        _loopSource.volume = value;
    }
}


