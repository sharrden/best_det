using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInput : MonoBehaviour
{
    public delegate void InteractAction();
    public static event InteractAction OnInteract;

    public delegate void SubmitAction();
    public static event SubmitAction OnSubmit;

    public delegate void MagnifyingGlassActivate();
    public static event MagnifyingGlassActivate OnMagnifyingGlassActivate;

    [SerializeField]
    private CharacterController2D controller;
    [SerializeField]
    private MagnifyingGlassController glassController;
    [SerializeField]
    private AudioClip _glass_out;
    [SerializeField]
    private AudioClip _glass_in;

    public Animator _animator;

    public float runSpeed = 40f;

    float horizontalMove = 0f;

    bool jump, crouch;
    
    private void Update()
    {
        _animator.SetFloat("MovementSpeed", Mathf.Abs(horizontalMove));
    }
    private void Start()
    {
        glassController.DeactivateMagnifyingGlass();    
    }
    public void Jump(InputAction.CallbackContext context)
    {
        jump = true;
    }

    public void Move(InputAction.CallbackContext context)
    {
        Vector2 input = context.ReadValue<Vector2>();
        horizontalMove = input.x * runSpeed;
    }

    public void MagnifyingGlassPress(InputAction.CallbackContext context)
    {
        // isPressed == button down
        // !isPressed == button up
        bool isPressed = context.ReadValueAsButton();
        if (isPressed)
        {
            glassController.ActivateMagnifyingGlass();
            HideCursor();
            ///m-glass sound
            if (_glass_out != null)
                SoundManager.Instance.Playsound(_glass_out);
            if (OnMagnifyingGlassActivate != null)
                OnMagnifyingGlassActivate();
        }
        else
        {
            glassController.DeactivateMagnifyingGlass();
            if (_glass_out != null)
                SoundManager.Instance.Playsound(_glass_in);
            ShowCursor();
        }

    }

    private void HideCursor()
    {
        Cursor.visible = false;
    }

    private void ShowCursor()
    {
        Cursor.visible = true;
    }

    public void OnSubmitPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (OnSubmit != null)
                OnSubmit();
        }
    }

    public void OnInteractPressed(InputAction.CallbackContext context)
    {
        if (context.performed)
        {
            if (OnInteract != null)
                OnInteract();
        }
    }
    
    void FixedUpdate()
    {
        if (DialogueManager.GetInstance().isDialoguePlaying)
            return;

        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;
    }
}
