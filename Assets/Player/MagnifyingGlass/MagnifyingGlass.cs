using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MagnifyingGlass : MonoBehaviour
{
    Vector3 m_Point;
    CircleCollider2D m_Collider;
    public Tilemap m_tilemapActiveInside;
    public Tilemap m_tilemapActiveOuside;
    public LayerMask m_WhatToOverlap;

    bool isPlayerInside = false;
    private void Start()
    {
        m_Collider = GetComponent<CircleCollider2D>();
    }

    private void FixedUpdate()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(gameObject.transform.position, 1.5f, m_WhatToOverlap);    
        for (int i = 0; i < colliders.Length; i++)
        {
            Collider2D collider = colliders[i];
        }
    }
    private void OnCollisionStay2D(Collision2D other)
    {
    }

    private void OnTriggerStay(Collider other)
    {
    }
    void Update()
    {
    }

}
