using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragMouseMove : MonoBehaviour
{
    public GameObject objectToMove;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        objectToMove.transform.position = new Vector3(mousePosition.x, mousePosition.y, 0f);
    }
}
