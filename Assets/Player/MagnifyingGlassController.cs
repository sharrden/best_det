using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MagnifyingGlassController : MonoBehaviour
{
    public GameObject magnifyingGlass;
    bool isGlassActive = false;
    bool isGlassDirty = true; // "Does it need status update?"
    
    private void Start()
    {
        isGlassDirty = false;
        isGlassActive = false;
        DeactivateMagnifyingGlass();
    }
    
    public void ActivateMagnifyingGlass()
    {
        isGlassActive = true;
        magnifyingGlass.SetActive(true);
    }

    public void DeactivateMagnifyingGlass()
    {
        isGlassActive = false;
        magnifyingGlass.SetActive(false);
    }
}
