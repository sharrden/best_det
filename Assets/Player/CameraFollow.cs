using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform m_target;
    [Range(0, 1)]
    public float m_smoothSpeed = 0.125f;
    public Vector3 m_offset = new Vector3(0,2,-10);

    private void FixedUpdate()
    {
        Vector3 desiredPosition = m_target.position + m_offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, m_smoothSpeed);
        transform.position = smoothedPosition;
    }
}
