using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro_HouseEntry : MonoBehaviour
{
    [SerializeField] private GameObject m_butler;
    [SerializeField] private float m_butlerMoveSpeed = 0.5f;
    [SerializeField] private Transform m_targetPositionForButler;

    private bool m_shouldButlerMove = false;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            m_shouldButlerMove = true;
        }    
    }

    private void Update()
    {
        if (m_butler == null || !m_shouldButlerMove)
            return;
        Vector3 moveDirection = m_targetPositionForButler.position - m_butler.transform.position;
        Debug.Log(moveDirection.magnitude);
        if (moveDirection.magnitude < 1f)
        {
            m_shouldButlerMove = false;
            m_butler = null;
            //gameObject.SetActive(false);
        }
        if (m_shouldButlerMove)
        {
            m_butler.transform.Translate(moveDirection.normalized * Time.deltaTime * m_butlerMoveSpeed);
        }
    }
}
