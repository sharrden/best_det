using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intro_ReginaApproach : MonoBehaviour
{
    [SerializeField] private GameObject m_paintingToReplace;
    [SerializeField] private GameObject m_paintingNew;
    [SerializeField] private TextAsset m_revealedPaintingText;
    [SerializeField] private AudioClip _clothremove;
    [SerializeField] private GoToNextLevel m_levelSwitcher;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            DialogueManager.OnDialogFinished += OnDialogueFinished;
        }    
    }

    private void OnDialogueFinished()
    {
        DialogueManager.OnDialogFinished -= OnDialogueFinished;
        ReplacePaintings();
        StartCoroutine(WaitAndLaunchDialogue());
    }

    private void ReplacePaintings()
    {
        m_paintingNew.transform.position = m_paintingToReplace.transform.position;
        SoundManager.Instance.volume = 1;
        SoundManager.Instance.Playsound(_clothremove);
        Destroy(m_paintingToReplace);
        m_paintingToReplace = null;
    }

    private IEnumerator WaitAndLaunchDialogue()
    {
        yield return new WaitForSeconds(2);
        DialogueManager.GetInstance().EnterDialogueMode(m_revealedPaintingText);
        m_levelSwitcher.FinalDialogueStarted();
    }
}
