using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravestoneTutorial : MonoBehaviour
{
    [SerializeField] private GameObject m_tutorialVisual;
    private bool m_alreadyWasActivated = false;
    private void Start()
    {
        m_tutorialVisual.SetActive(false);    
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        DialogueManager.OnDialogFinished += OnDialogueFinished; 
    }

    private void OnDialogueFinished()
    {
        DialogueManager.OnDialogFinished -= OnDialogueFinished;
        PlayerInput.OnMagnifyingGlassActivate += OnGlassActivate;
        m_tutorialVisual.SetActive(true);
    }

    private void OnGlassActivate()
    {
        if (m_alreadyWasActivated)
            return;
        m_alreadyWasActivated = true;
        PlayerInput.OnMagnifyingGlassActivate -= OnGlassActivate;
        Debug.Log("glass activated");
        WaitAndHideGlass();
    }

    private IEnumerator WaitAndHideGlass()
    {
        yield return new WaitForSeconds(2);
        m_tutorialVisual.SetActive(false);
    }
}
