using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroStoryDriver : MonoBehaviour
{
    [SerializeField] private GameObject m_entrancePreventer;
    [SerializeField] private TextAsset m_inkJSONinFrontDoor;
    [SerializeField] private AudioClip _knock;
    [SerializeField] private AudioClip _strangemusic;

    private void OnEnable()
    {
        DialogTrigger.OnDialogInitiated += OnDialogStarted;       
    }
    private void OnDisable()
    {
        DialogTrigger.OnDialogInitiated -= OnDialogStarted;    
    }

    private void OnDialogStarted(TextAsset inkJSON)
    {
        if (inkJSON == m_inkJSONinFrontDoor)
        {
            SoundManager.Instance.volume = 1;
            SoundManager.Instance.Playsound(_knock);
            SoundManager.Instance.Playmusic(_strangemusic);
            Destroy(m_entrancePreventer);
            m_entrancePreventer = null;
        }
    }
}
