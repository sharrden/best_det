using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoToNextLevel : MonoBehaviour
{
    public bool m_finalDialogStarted = false;
    public void FinalDialogueStarted()
    {
        DialogueManager.OnDialogFinished += OnDialogueFinished;
    }

    private void OnDialogueFinished()
    {   
        DialogueManager.OnDialogFinished -= OnDialogueFinished;
        StartCoroutine(WaitAndGoToLevel1());
    }

    private IEnumerator WaitAndGoToLevel1()
    {
        Debug.Log("wait and go");
        yield return new WaitForSeconds(3);
        Debug.Log("Go");
        SceneManager.LoadScene("Level1");
    }
}
