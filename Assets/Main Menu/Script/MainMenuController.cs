using UnityEngine;
using UnityEngine.SceneManagement;


public class MainMenuController : MonoBehaviour

{
    //public AudioSource audioSource;

     void Start()
    {
       // audioSource = GetComponent<AudioSource>();
    }
    // Start is called before the first frame update
    public void StartGame()
    {
        SceneManager.LoadScene("Intro");
    }

    public void LoadMainMenu()
    {
        SceneManager.LoadScene("MainMenu");

    }
    public void QuitGame()
    {
        Application.Quit();
    }
    // Update is called once per frame
    void Update()
    {

    }
}
