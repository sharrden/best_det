using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTrigger : MonoBehaviour
{
    enum GlassVisibility
    {
        VisibleInside, VisibleOutside
    }

    bool m_playerInRange = false;
    bool m_insideMagnifyingGlass = false;

    [Header("Visual Cue")]
    [SerializeField] private GameObject m_visualCue;
    [Header("Magnifying glass interaction")]
    [SerializeField] private GlassVisibility m_visibilityType =
        GlassVisibility.VisibleOutside;


    private void Update()
    {
        if (m_visualCue.activeInHierarchy && !IsCurrentlyInteractable())
            m_visualCue.SetActive(false);
        else if (!m_visualCue.activeInHierarchy && IsCurrentlyInteractable())
            m_visualCue.SetActive(true);    
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && !m_playerInRange)
        {
            PlayerInput.OnInteract += OnInteractPressed;
            m_playerInRange = true;
        }
        if (other.gameObject.tag == "MagGlass")
        {
            m_insideMagnifyingGlass = true;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && m_playerInRange)
        {
            PlayerInput.OnInteract -= OnInteractPressed;
            m_playerInRange = false;
        }
        if (other.gameObject.tag == "MagGlass")
        {
            m_insideMagnifyingGlass = false;
        }
    }

    private void OnInteractPressed()
    {
        if (IsCurrentlyInteractable())
        {
            Debug.Log("Interacting with " + gameObject.name);
        }
    }

    private bool IsCurrentlyInteractable()
    {
        if (!m_playerInRange)
            return false;
        if (m_visibilityType == GlassVisibility.VisibleInside &&
            m_insideMagnifyingGlass)
            return true;
        if (m_visibilityType == GlassVisibility.VisibleOutside &&
            !m_insideMagnifyingGlass)
            return true;
        return false;
    }
}
