using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CleaningCabinet : MonoBehaviour
{
    [SerializeField] private TextAsset m_inkJSON_ashbinFoundKey;
    [SerializeField] private GameObject m_CleaningCabinetOpened;

    private bool m_hasPlayerFoundKey = false;

    private void OnEnable()
    {
        DialogTrigger.OnDialogInitiated += OnDialogStarted;
        m_CleaningCabinetOpened.SetActive(false);
    }

    private void OnDisable()
    {
        DialogTrigger.OnDialogInitiated -= OnDialogStarted;
    }

    private void OnDialogStarted(TextAsset inkJSON)
    {
        if (inkJSON == m_inkJSON_ashbinFoundKey)
        {
            DialogTrigger.OnDialogInitiated -= OnDialogStarted;
            m_hasPlayerFoundKey = true;
            m_CleaningCabinetOpened.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
