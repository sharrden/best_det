using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Diary : MonoBehaviour
{
    [SerializeField] private TextAsset m_inkJSONregina;
    [SerializeField] private TextAsset m_inkJSONbutler;
    [SerializeField] private TextAsset m_inkJSONdiaryOpen;
    [SerializeField] private GameObject npc_openDiary;

    private int cluesLeft = 2;

    private void OnEnable()
    {
        npc_openDiary.SetActive(false);
        DialogTrigger.OnDialogInitiated += OnDialogStarted;
    }
    private void OnDisable()
    {
        DialogTrigger.OnDialogInitiated -= OnDialogStarted;
    }

    private void OnDialogStarted(TextAsset inkJSON)
    {
        Debug.Log(inkJSON);
        if (inkJSON == m_inkJSONregina)
        {
            cluesLeft -= 1;
            Debug.Log(cluesLeft);
        }

        Debug.Log(inkJSON);
        if (inkJSON == m_inkJSONbutler)
        {
            cluesLeft -= 1;
            Debug.Log(cluesLeft);
        }

        if (cluesLeft == 0)
        {
            DialogueManager.OnDialogFinished += OnDialogueFinished;
            npc_openDiary.SetActive(true);
            gameObject.SetActive(false);

        }
    }
    private void OnDialogueFinished()
    {
        DialogueManager.OnDialogFinished -= OnDialogueFinished;
        
    }
}
