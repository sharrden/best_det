using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ashbin : MonoBehaviour
{
    [SerializeField] private TextAsset m_inkJSON_maris;
    [SerializeField] private GameObject m_ashbinWithKey;

    private void OnEnable()
    {
        m_ashbinWithKey.SetActive(false);
        DialogTrigger.OnDialogInitiated += OnDialogStarted;
    }
    private void OnDisable()
    {
        DialogTrigger.OnDialogInitiated -= OnDialogStarted;
    }

    private void OnDialogStarted(TextAsset inkJSON)
    {
        if (inkJSON == m_inkJSON_maris)
        {
            DialogueManager.OnDialogFinished += OnDialogueFinished;
            m_ashbinWithKey.SetActive(true);
            gameObject.SetActive(false);

        }
    }
    private void OnDialogueFinished()
    {
        DialogueManager.OnDialogFinished -= OnDialogueFinished;
        
    }
}
