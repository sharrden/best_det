->DiaryHint1
===DiaryHint1===
THE BUTLER
I have served the Doyle family for decades. I like to think I know them better than they know themselves! They all have their own quirks.
->DiaryHint2
===DiaryHint2===
THE BUTLER
For example, Regina has six toes in her left foot, Maris is terrified of penguins and Edgar only drinks wines bottled in the year 1477. How peculiar!
->END