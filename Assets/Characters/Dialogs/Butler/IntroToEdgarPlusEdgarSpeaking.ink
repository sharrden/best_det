-> introduction

=== introduction ===

THE BUTLER
Excuse me ma’am, the detective is here. Detective, I present to you lady Regina Doyle and her brother, mister Edgar Doyle.
-> edgar1

=== edgar1 ===
EDGAR DOYLE
Hmph, we couldn’t get mister Holmes? Still, I am happy you are finally here to find the culprit responsible for this nightmare.
-> edgar2
=== edgar2 ===
EDGAR DOYLE
I cannot imagine what sick individual could do something like this. To not only destroy, but to absolutely desecrate their victim!
-> edgar3
=== edgar3 ===
EDGAR DOYLE
It's horrifying. We covered the mess, it was too horrible to watch. Remove the cloth, if you dare...
->END