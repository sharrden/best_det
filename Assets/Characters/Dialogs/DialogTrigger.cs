using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogTrigger : MonoBehaviour
{
    // Magnifying glass interaction
    enum GlassVisibility
    {
        VisibleInside, VisibleOutside
    }
bool m_insideMagnifyingGlass = false;
[SerializeField] private GlassVisibility m_visibilityType =
        GlassVisibility.VisibleOutside;
    //--------


    public delegate void DialogInitiated(TextAsset inkJSON);
    public static event DialogInitiated OnDialogInitiated;

    [Header("Visual Cue")]
    [SerializeField] private GameObject m_visualCue;

    [Header("Ink JSON")]
    [SerializeField] private TextAsset m_inkJSON;
    [SerializeField] private bool m_shouldStartWithoutExplicitInteraction = false;
    [SerializeField] private int m_timesCanPlay = 1;
    [SerializeField] private bool m_captureTriggerStay = false;

    private int m_timesToPlayMax;
    private bool m_playerInRange;

    private void Awake()
    {
        m_playerInRange = false;
        if (m_visualCue != null)
            m_visualCue.SetActive(false);
        m_timesToPlayMax = m_timesCanPlay;
    }
    private void Update()
    {
        if (m_visualCue != null)
        {
            if (m_visualCue != null && m_visualCue.activeInHierarchy && 
            DialogueManager.GetInstance().isDialoguePlaying &&
            !IsCurrentlyInteractable())
            {
                m_visualCue.SetActive(false);
            }
            else if (!m_visualCue.activeInHierarchy && IsCurrentlyInteractable() &&
            !DialogueManager.GetInstance().isDialoguePlaying)
            {
                m_visualCue.SetActive(true);
            }
        }
    }

    private void OnInteractPressed()
    {
        if (m_playerInRange && !DialogueManager.GetInstance().isDialoguePlaying
        && IsCurrentlyInteractable())
        {
            PerformDialogStart();
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (m_timesToPlayMax > 0 &&
         other.gameObject.tag == "Player" && !m_playerInRange)
        {
            PlayerInput.OnInteract += OnInteractPressed;
            m_playerInRange = true;
            if (m_visualCue != null)
                m_visualCue.SetActive(true);
            if (m_shouldStartWithoutExplicitInteraction)
                PerformDialogStart();
        }
        if (other.gameObject.tag == "MagGlass")
        {
            m_insideMagnifyingGlass = true;
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (m_timesToPlayMax > 0 &&
         other.gameObject.tag == "Player" && !m_playerInRange)
        {
            PlayerInput.OnInteract += OnInteractPressed;
            m_playerInRange = true;
            if (m_visualCue != null)
                m_visualCue.SetActive(true);
            if (m_shouldStartWithoutExplicitInteraction)
                PerformDialogStart();
        }
    }
    private void PerformDialogStart()
    {
        m_timesToPlayMax--;
        if (m_timesToPlayMax >= 0)
        {
            DialogueManager.GetInstance().EnterDialogueMode(m_inkJSON);
            if (OnDialogInitiated != null)
                OnDialogInitiated(m_inkJSON);
        }
        
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player" && m_playerInRange)
        {
            PlayerInput.OnInteract -= OnInteractPressed;
            m_playerInRange = false;
            if (m_visualCue != null)
                m_visualCue.SetActive(false);
        }
        if (other.gameObject.tag == "MagGlass")
        {
            m_insideMagnifyingGlass = false;
        }
    }

    private bool IsCurrentlyInteractable()
    {
        if (!m_playerInRange)
            return false;
        if (m_visibilityType == GlassVisibility.VisibleInside &&
            m_insideMagnifyingGlass)
            return true;
        if (m_visibilityType == GlassVisibility.VisibleOutside &&
            !m_insideMagnifyingGlass)
            return true;
        return false;
    }
}
