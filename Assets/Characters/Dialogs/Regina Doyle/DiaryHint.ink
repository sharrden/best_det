->hint1
===hint1===
REGINA DOYLE
You know, my brother is very private by nature, doesn't really share his feelings and thoughts... But I don't mind!
->hint2
===hint2===
REGINA DOYLE
I can always read them from his diary! Haha! It's easy enough to open if you know his favourite wine.
->END
