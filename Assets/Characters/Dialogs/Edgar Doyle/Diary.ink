->detective1
===detective1===
The combination was 1477. Let's see...

->diary1
===diary1===
DEAR DIARY
I must admit, I wasn’t too sad when we lost my dear brother-in-law to that brutal accident. He used to enjoy pulling the most foul pranks on poor old me... 
->diary2
===diary2===
...startling me by jumping from around the corner or putting worms in my porridge. Good riddance, I say! But still his portrait in the salon haunts me. Oh how I hate that painting. Well, that's all for today, good night!
->detective2
===detective2===
...so, Edgar really hated the painting. That's interesting.
->END