using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Ink.Runtime;
using UnityEngine.EventSystems;

public class DialogueManager : MonoBehaviour
{
    public delegate void DialogFinished();
    public static event DialogFinished OnDialogFinished;

    [Header("Dialog UI")]
    [SerializeField] private GameObject m_dialoguePanel;
    [SerializeField] private TextMeshProUGUI m_dialogueText;
    [Header("Choices UI")]
    [SerializeField] private GameObject[] m_choices;
    private TextMeshProUGUI[] m_choicesText;

    private static DialogueManager m_instance;
    private Story m_currentStory;
    public bool isDialoguePlaying { get; private set; }
    private bool m_choiceMade = false;

    private void OnEnable()
    {
        PlayerInput.OnSubmit += ContinueStory;
    }

    private void Awake()
    {
        if (m_instance != null)
        {
            Debug.LogWarning("Found more than one DialogManager in the scene");
        }
        m_instance = this;
    }

    public static DialogueManager GetInstance()
    {
        return m_instance;
    }

    private void Start()
    {
        isDialoguePlaying = false;
        m_dialoguePanel.SetActive(false);

        m_choicesText = new TextMeshProUGUI[m_choices.Length];
        int index = 0;
        foreach(GameObject choice in m_choices)
        {
            m_choicesText[index] = choice.GetComponentInChildren<TextMeshProUGUI>();
            index++;
        }
    }

    public void EnterDialogueMode(TextAsset inkJSON)
    {
        m_currentStory = new Story(inkJSON.text);
        isDialoguePlaying = true;
        m_dialoguePanel.SetActive(true);

        ContinueStory();
    }

    private void ExitDialogueMode()
    {
        isDialoguePlaying = false;
        m_dialoguePanel.SetActive(false);
        m_dialogueText.text = "";
        if (OnDialogFinished != null)
            OnDialogFinished();
    }

    public void ContinueStory()
    {
        if (m_currentStory.canContinue && 
        // either we have no choices or we have and made one
            (m_currentStory.currentChoices.Count == 0 || m_choiceMade))
        {
            m_choiceMade = false;
            m_dialogueText.text = m_currentStory.Continue();
            DisplayChoices();
        }
        else
        {
            ExitDialogueMode();
        }
    }

    private void DisplayChoices()
    {
        List<Choice> currentChoices = m_currentStory.currentChoices;

        if (currentChoices.Count > m_choices.Length)
        {
            Debug.LogError("More choices were given than the UI can support. Number of choices given: "
            + currentChoices.Count);
        }

        int index = 0;
        foreach(Choice choice in currentChoices)
        {
            m_choices[index].gameObject.SetActive(true);
            m_choicesText[index].text = choice.text;
            index++;
        }

        // go through the remaining choices the UI supports and make sure they're hidden
        for (int i = index; i < m_choices.Length; i++)
        {
            m_choices[i].gameObject.SetActive(false);
        }
        //StartCoroutine(SelectFirstChoice());
    }

    private IEnumerator SelectFirstChoice()
    {
        // Event system requires we clear it first and set it in a different frame
        EventSystem.current.SetSelectedGameObject(null);
        yield return new WaitForEndOfFrame();
        EventSystem.current.SetSelectedGameObject(m_choices[0].gameObject);
    }

    public void MakeChoice(int choiceIndex)
    {
        m_currentStory.ChooseChoiceIndex(choiceIndex);
        m_choiceMade = true;
        ContinueStory();
    }
}
