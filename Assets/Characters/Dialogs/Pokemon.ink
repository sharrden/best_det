-> main

=== main ===
Soon you will be presented with pokemon choice
-> pokemonChoice

=== pokemonChoice ===
Which pokemon do you choose?
    +[Charmander]
        -> chosen("Charmander")
    +[Bulbasar]
        -> chosen("Bulbasar")
    +[Squirtle]
        -> chosen("Squirtle")
        
=== chosen(pokemon) ===
You chose {pokemon}!
-> END