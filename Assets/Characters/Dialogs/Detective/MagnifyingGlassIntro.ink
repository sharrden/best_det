->introObject
===introObject===
Hmm, a gravestone... Maybe I should look at it with my magnifying glass.

->magnifyingGlass
===magnifyingGlass===
As I suspected, it's fairly new. 

->magnifyingGlass2
===magnifyingGlass2===

Still, after all these years, it's hard to believe that a normal looking magnifying glass could possess the power to bend the time-space-continuum. 
->magnifyingGlass3
===magnifyingGlass3===
To think I almost threw it away when cleaning my late grandfather's attic, before I realised its mystical power.
->END