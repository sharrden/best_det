->talk1
===talk1===
Hmm, I think I have it all figured out now… After carefully inspecting the case, I have found not one, but two people responsible for this gruesome crime. The guilty ones are…
->talk2
===talk2===
…the cleaner and the gardener!! Whoa! Who would have guessed! Well I did of course, because I’m such a clever detective. Ahem.
->talk3
===talk3===
You see, miss Maris Doyle is always complaining that the cleaner doesn’t wash the floors properly. So, when the gardener walked in the salon with her dirty boots, he was naturally mad, and ordered her to clean them immediately.
->talk4
===talk4===
The gardener obeyed and scrubbed her boots with some rags and shoe polish. She was told to put the dirty rags to an ash bin next to the fireplace, but it wasn’t there. Too scared to ask the already angry cleaner for further instructions, she left them on a table under the painting and left. The cleaner didn’t notice any of this since he was too busy mopping the floors elsewhere.
->talk5
===talk5===
The thing about shoe polish is, it catches fire very easily. It only took a ray of sunshine coming through the window, and they burst into flames. Foom! And the flames reached the painting! When our criminal duo saw this, they put out the fire, but it was too late. The painting was ruined. They saw no other option but to try to cover the whole thing up by trying to fix the painting, poorly.
->talk6
===talk6===
It’s ironic, really. The culprits didn’t mean to destroy the precious portrait and even did their best to undo the damage. What’s even more ironic is that the reason the ash bin was missing, is that I used my magical time travelling magnifying glass to take it from the past to present time, causing a time travelling paradox! So… maybe it’s me who’s guilty.
->talk7
===talk7===
Well, I’ll just go and lock myself in jail now. Thank you all, and goodbye!
->END

